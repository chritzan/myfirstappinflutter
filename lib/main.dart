import 'package:flutter/material.dart';
import 'package:meditation_app/login_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Meditatin App',
      theme: ThemeData(
        splashColor: Colors.blue,
        highlightColor: Colors.white.withOpacity(.5),
        appBarTheme: AppBarTheme(
          backgroundColor: Colors.pink.shade100,
          foregroundColor: Colors.black,
        ),
      ),
      home: const LoginScreen(),
    );
  }
}

//bit bucket link repository
//https://bitbucket.org/chritzan/myfirstappinflutter/src/master/
