import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
// ignore: import_of_legacy_library_into_null_safe
//import 'package:multi_select_item/multi_select_item.dart';
//import 'package:multi_select_flutter/multi_select_flutter.dart';

import 'package:meditation_app/slidable_action.dart';

class RandomWords extends StatefulWidget {
  const RandomWords({Key? key}) : super(key: key);

  @override
  _RandomWordsState createState() => _RandomWordsState();
}

class _RandomWordsState extends State<RandomWords> {
  final _suggestions = <WordPair>[];
  final _saved = <WordPair>{};
  final _biggerFont = const TextStyle(fontSize: 18.0);

  var isSelected = false;
  var mycolor = Colors.white;

  Future<bool> _onBackPressed() async {
    return (await showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: const Text('Are you sure?'),
            content: const Text('Do you want to exit the App'),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: const Text('No'),
              ),
              TextButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: const Text('Yes'),
              ),
            ],
          ),
        )) ??
        false;
  }
  /*
var myMultiSelectController = MultiSelectController();
  
  @override
  void initState() {
    super.initState();
    myMultiSelectController.disableEditingWhenNoneSelected = true;
    myMultiSelectController.set(_suggestions.length);
  } 

  void selectAllItems() {
    setState(() {
      myMultiSelectController.toggleAll();
    });
  }

  void deleteItems() {
    var list = myMultiSelectController.selectedIndexes;
    list.sort((b, a) => a.compareTo(b));
    for (var element in list) {
      _suggestions.removeAt(element);
    }
    setState(() {
      myMultiSelectController.set(_suggestions.length);
    });
  }*/

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        appBar: AppBar(
          /* actions: (myMultiSelectController.isSelecting)
              ? <Widget>[
                  Text('(${_suggestions.length})'),
                  IconButton(
                    icon: const Icon(Icons.select_all),
                    onPressed: selectAllItems,
                    tooltip: 'SelectAll',
                  ),
                  IconButton(
                    icon: const Icon(
                      Icons.delete,
                      color: Colors.red,
                    ),
                    tooltip: 'Delete',
                    onPressed: deleteItems,
                  )
                ]
              : <Widget>[],*/
          centerTitle: true,
          title: const Text('Words Combination'),
        ),
        body: Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("./asset/christ.jfif"), fit: BoxFit.cover),
            ),
            child: _buildSuggestions()),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.white,
          onPressed: _pushSaved,
          tooltip: 'Delete',
          child: Column(
            children: [
              const Icon(
                Icons.favorite,
                color: Colors.red,
              ),
              Text(
                '(${_saved.length})',
                style: const TextStyle(
                  color: Colors.black,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSuggestions() {
    return Expanded(
      child: ListView.builder(
          padding: const EdgeInsets.all(16.0),
          itemBuilder: (
            context,
            i,
          ) {
            if (i.isOdd) return const Divider();

            final indexs = i ~/ 2;
            if (indexs >= _suggestions.length) {
              _suggestions.addAll(generateWordPairs().take(10));
            }

            return Slidable(
              child: GestureDetector(
                onTap: () {
                  setState(() {});
                },
                onLongPress: toggleSelection,
                child: Container(
                  color: (indexs % 2 == 0)
                      ? const Color.fromRGBO(163, 225, 255, 19)
                      : const Color.fromRGBO(63, 255, 255, 90),
                  child: Center(
                    child: _buildRow(_suggestions[indexs]),
                  ),
                ),
              ),
              actionPane: const SlidableDrawerActionPane(),
              actionExtentRatio: 0.2,
              // left side
              actions: <Widget>[
                IconSlideAction(
                  caption: 'Delete',
                  color: Colors.red,
                  icon: Icons.delete,
                  onTap: () => onDismissed(indexs, SlidableAction.delete),
                ),
              ],
              //right side
              secondaryActions: <Widget>[
                IconSlideAction(
                  caption: 'Share',
                  color: Colors.green,
                  icon: Icons.edit,
                  onTap: () {},
                ),
              ],
            );
          }),
    );
  }

  void onDelete(int indexs) {
    setState(() => _suggestions.removeAt(indexs));
  }

  void onDismissed(int indexs, SlidableAction action) {
    setState(() => _suggestions.removeAt(indexs));
  }

  Widget _buildRow(WordPair pair) {
    final alreadySaved = _saved.contains(pair);

    return Card(
      color: mycolor,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        children: [
          ListTile(
            selected: isSelected,
            onLongPress: toggleSelection,
            title: Text(
              pair.asPascalCase,
              style: _biggerFont,
            ),
            trailing: alreadySaved
                ? const Icon(
                    Icons.favorite,
                    color: Colors.red,
                  )
                : const Icon(
                    Icons.favorite_border,
                    color: Colors.black,
                  ),
            onTap: () {
              setState(() {
                if (alreadySaved) {
                  _saved.remove(pair);
                } else {
                  _saved.add(pair);
                }
              });
            },
          ),
        ],
      ),
    );
  }

  void toggleSelection() {
    setState(() {
      if (isSelected) {
        BoxDecoration(border: Border.all(color: Colors.white));
        mycolor = Colors.white;
        isSelected = false;
      } else {
        BoxDecoration(border: Border.all(color: Colors.grey));
        mycolor = Colors.grey;
        isSelected = true;
      }
    });
  }

  void _pushSaved() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (context) {
          final tiles = _saved.map(
            (pair) {
              return ListTile(
                onTap: () {},
                onLongPress: () {},
                title: Text(
                  pair.asPascalCase,
                  style: _biggerFont,
                ),
              );
            },
          );
          final divided = tiles.isNotEmpty
              ? ListTile.divideTiles(
                  context: context,
                  tiles: tiles,
                ).toList()
              : <Widget>[];

          return Scaffold(
            backgroundColor: Colors.red[70],
            appBar: AppBar(
              title: const Text('Saved Suggestions'),
              centerTitle: true,
            ),
            body: Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("./asset/yah.jfif"), fit: BoxFit.cover),
                ),
                child: ListView(children: divided)),
          );
        },
      ),
    );
  }
}
