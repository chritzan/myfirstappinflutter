/*
MultiSelectController myMultiSelectController = MultiSelectController();
  @override
  void initState() {
    super.initState();
    myMultiSelectController.disableEditingWhenNoneSelected = true;
    myMultiSelectController.set(_suggestions.length);
  }
 AppBar(
title: Text(‘Flutter Multi Select List Items’),
actions: (myMultiSelectController.isSelecting)
? <Widget>[
IconButton(
icon: Icon(Icons.select_all),
onPressed: selectAllItems,
),
IconButton(
icon: Icon(Icons.delete),
onPressed: deleteItems,
)
]
: <Widget>[]
), 

  void selectAllItems() {
    setState(() {
      myMultiSelectController.toggleAll();
    });
  }

  void deleteItems() {
    var list = myMultiSelectController.selectedIndexes;
    list.sort((b, a) => a.compareTo(b));
    for (var element in list) {
      _suggestions.removeAt(element);
    }
    setState(() {
      myMultiSelectController.set(_suggestions.length);
    });
  }

 ListView.builder(
itemCount: listPopularFootballClubs.length,
itemBuilder: (context, index) {
return Container(
margin: EdgeInsets.symmetric(horizontal: 15, vertical: 8),
height: 90,
decoration: BoxDecoration(
color: Colors.transparent,
borderRadius: BorderRadius.circular(10),
),
child: InkWell(
onTap: () {},
child: Container(
child: MultiSelectItem(
isSelecting: myMultiSelectController.isSelecting,
onSelected: () {
setState(() {
myMultiSelectController.toggle(index);
});
},
child: Card(
color: myMultiSelectController.isSelected(index)
? Colors.grey.shade400
: Colors.white,
shape: RoundedRectangleBorder(
borderRadius: BorderRadius.all(Radius.circular(7)),
),
child: Center(
child: Text(listPopularFootballClubs[index],
style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
),
),
)
),
),
);
},
), */ 

// solution
/*import 'package:flutter/material.dart';

class cardy extends StatefulWidget {
  @override
  _cardyState createState() => new _cardyState();
}

class _cardyState extends State<cardy> {
  var isSelected = false;
  var mycolor=Colors.white;

  @override
  Widget build(BuildContext context) {
    return new Card(
      color: mycolor,
      child: new Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
        new ListTile(
            selected: isSelected,
            leading: const Icon(Icons.info),
            title: new Text("Test"),
            subtitle: new Text("Test Desc"),
            trailing: new Text("3"),
            onLongPress: toggleSelection // what should I put here,
            )
      ]),
    );
  }

  void toggleSelection() {
    setState(() {
      if (isSelected) {
        border=new BoxDecoration(border: new Border.all(color: Colors.white));
        mycolor = Colors.white;
        isSelected = false;
      } else {
        border=new BoxDecoration(border: new Border.all(color: Colors.grey));
        mycolor = Colors.grey[300];
        isSelected = true;
      }
    });
  }
}*/
